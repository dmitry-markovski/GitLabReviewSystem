==ADDING SOME TEXT BELOW JUST TO TEST THE REVIEW SYSTEM

Command line instructions

Git global setup

git config --global user.name "Dmitry Markovski"
git config --global user.email "dmitry.markovski@gmail.com"

Create a new repository

git clone https://gitlab.com/dmitry-markovski/GitLabReviewSystem.git
cd GitLabReviewSystem
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/dmitry-markovski/GitLabReviewSystem.git
git push -u origin master
